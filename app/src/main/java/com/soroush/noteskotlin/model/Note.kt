package com.soroush.noteskotlin.model

import android.provider.ContactsContract
import com.google.gson.annotations.SerializedName


data class Note(
    @SerializedName("text") var text: String
)