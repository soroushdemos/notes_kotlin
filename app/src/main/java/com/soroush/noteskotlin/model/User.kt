package com.soroush.noteskotlin.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class User(
    @SerializedName("notes") var notes: List<Note>? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("name") var name: String? = null,
    @SerializedName("lastModified") var lastModified: Date? = null
)