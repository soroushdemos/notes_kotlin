package com.soroush.noteskotlin.api

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = {ServiceProvider.class} )
interface ServiceComponent {
    fun inject()
}