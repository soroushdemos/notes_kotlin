package com.soroush.noteskotlin.api

import retrofit2.http.*

interface UserService : BaseService {
    @GET("users/login}")
    fun loginUser(
        @Query("username") username: String,
        @Query("password") password: String
    )

    @GET("users/{username}")
    fun getUser(@Path("user") username: String)

    @PUT("users/{username}")
    fun updateUser(@Path("user") username: String)

    @POST("users/{username}")
    fun signupUser(@Path("user") username: String)
}