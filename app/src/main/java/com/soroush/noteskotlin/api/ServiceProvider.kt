package com.soroush.noteskotlin.api

import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module

class ServiceProvider {
    private val BASE_URL = "http://10.0.2.2:3000/"
    private val AUTH = ""

    @Provides
    @Reusable
    internal fun provideUserService(): UserService = retrofit.create(UserService::class.java)


    /*****************
     * Helper Methods
     *****************/
    private val okHttpClient = OkHttpClient.Builder()
        .addInterceptor { chain ->
            val original = chain.request()

            val requestBuild = original.newBuilder()
                .addHeader("Authorization", AUTH)
                .method(original.method(), original.body())

            val request = requestBuild.build()
            chain.proceed(request)
        }.build()


    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
}